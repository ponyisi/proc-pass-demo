from xmlrpc.server import DocXMLRPCServer
from xmlrpc.server import DocXMLRPCRequestHandler
import json

# Restrict to a particular path.
class RequestHandler(DocXMLRPCRequestHandler):
    rpc_paths = ('/RPC2',)

with open('/etc/config/atlasdqm_config_eos.json') as fconfig:
    mconfig = json.load(fconfig)
    print(mconfig)

HANDIRS = mconfig['HANDIRS']

def get_next_proc_pass(run, stream, source):
    """
This function is intended for internal DQMF infrastructure use.

Usage: get_next_proc_pass(run, stream, source)

Arguments:

    * run: run as an integer
    * stream: stream as a string
    * source: DQMF source as a string; valid options are 'tier0' and 'reproc' 

Returns:

    * integer indicating the value that should be used as the next 'processing
      pass' variable. 
    """
    if source not in HANDIRS:
        raise ValueError('%s is not a known source of web display data'
                         % source)
    topdir = HANDIRS[source]
    import glob, re
    fmatches = glob.glob('%s/*/%s/run_%s' % (topdir, stream, run))
    ex = re.compile('%s\/([0-9]*?)/%s' % (topdir, stream))
    matches = [ex.match(i) for i in fmatches]
    matches = [int(i.group(1)) for i in matches if i != None and i.group(1) != None]
    if len(matches) == 0:
        return 1
    else:
        return max(matches) + 1

if __name__ == '__main__':
    # Create server
    server = DocXMLRPCServer(("", 8081),
                                requestHandler=RequestHandler)
    server.register_introspection_functions()
    server.register_multicall_functions()
    server.set_server_title('atlasdqm')
    server.set_server_name('ATLAS Data Quality Processing Pass Service')
    server.set_server_documentation("""

        """)

    server.register_function(get_next_proc_pass)

    def vararg(*args):
        return args
    server.register_function(vararg)

    # Run the server's main loop
    print('Set up and ready to go')
    server.serve_forever()